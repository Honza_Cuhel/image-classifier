import tensorflow as tf
import tensorflow_datasets as tfds
import tensorflow_hub as hub
import PIL.Image as Image
import numpy as np
import os
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import logging
logging.getLogger("tensorflow").setLevel(logging.WARNING)

CLASSIFIER_URL = "https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4"  # MobileNet v4
IMG_SIZE = 224
IMG_COLOR = 255.0
BATCH_SIZE = 32
IMAGE_FROM_NET = 1
IMAGE_FROM_PC = 0
SAVED_MODEL_PATH = "./model"


def format_image(img, label):
    img = tf.image.resize(img, (IMG_SIZE, IMG_SIZE))/IMG_COLOR
    return img, label


class ImageClassifier:
    def __init__(self, number_of_classes, epochs=1000, saved_model_path=SAVED_MODEL_PATH, class_names=[]):
        feature_extractor = hub.KerasLayer(CLASSIFIER_URL, input_shape=(IMG_SIZE, IMG_SIZE, 3))
        feature_extractor.trainable = False

        self.model = tf.keras.Sequential([
            feature_extractor,
            tf.keras.layers.Dense(units=number_of_classes, activation='softmax')
        ])
        self.model.compile(optimizer='adam',
                           loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                           metrics=['accuracy'])
        self.history = []
        self.info = []
        self.epochs = epochs
        self.saved_model_path = saved_model_path
        self.class_names = class_names

    def train(self, dataset_name):
        # Get the dataset
        (training_set, validation_set), info = tfds.load(
            dataset_name,
            split=[
                tfds.Split.TRAIN.subsplit(tfds.percent[:80]),
                tfds.Split.TRAIN.subsplit(tfds.percent[80:])
            ],
            # split=['train[:80%]', 'train[80%:]'],
            with_info=True,
            as_supervised=True,
        )

        # Print info
        # print(info)
        self.info = info

        # Get the number of train examples
        n = info.splits['train'].num_examples

        # Split the datasets to batches and formats the images
        train_batches = training_set.shuffle(n//4).map(format_image).batch(BATCH_SIZE).prefetch(1)
        validation_batches = validation_set.map(format_image).batch(BATCH_SIZE).prefetch(1)

        self.history = self.model.fit(train_batches,
                                        epochs=self.epochs,
                                        validation_data=validation_batches)
                                        # callbacks=tf.keras.callbacks.EarlyStopping(patience=10))

    def predict(self, URL, mode=IMAGE_FROM_PC, file_name='filename'):
        # Get the image
        if mode == IMAGE_FROM_PC:
            img = tf.keras.preprocessing.image.load_img(URL)
            img = np.array(img.resize((IMG_SIZE, IMG_SIZE))) / IMG_COLOR
        else:
            img = Image.open(tf.keras.utils.get_file(file_name, URL)).resize((IMG_SIZE, IMG_SIZE))
            img = np.array(img)/IMG_COLOR

        # Make the prediction
        result = self.model.predict(img[np.newaxis, ...])

        # Get the predicted class index
        predicted_idx = np.argmax(result, axis=-1)

        # Get the classes
        if len(self.class_names) == 0:
            self.class_names = np.array(self.info.features['label'].names)

        # Return the name of the predicted class
        return self.class_names[predicted_idx][0]

    def save(self):
        tf.saved_model.save(self.model, self.saved_model_path)
        print("Saved")

    def load(self):
        if os.path.isdir(self.saved_model_path):
            self.model = tf.keras.models.load_model(
                  self.saved_model_path,
                  custom_objects={'KerasLayer': hub.KerasLayer}
            )
            print('Model was successfully loaded.')
        else:
            print('Saved model ', self.saved_model_path, ' does not exists!')

    def print_model(self):
        print(self.model.summary())


if __name__ == "__main__":
    cat_dogs_classifier = ImageClassifier(2, epochs=2, saved_model_path="./cat_vs_dog_model",
                                          class_names=np.array(['cat', 'dog']))

    # Load the already trained model
    cat_dogs_classifier.load()

    # Train
    # cat_dogs_classifier.train('cats_vs_dogs')

    # Predict Cat
    print(cat_dogs_classifier.predict('./images/cat.jpg'), ' should be cat')

    # Predict dog
    print(cat_dogs_classifier.predict('./images/dog.jpg'), ' should be dog')

    print(cat_dogs_classifier.predict('https://www.holidaycat.cz/wp-content/uploads/2015/04/cute-cat2.jpg',
                                      IMAGE_FROM_NET, 'cat2.jpg'), ' should be cat')
    # Save the model
    # cat_dogs_classifier.save()
